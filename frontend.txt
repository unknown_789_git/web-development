#Front End Development

 The part of a website that user interacts with directly is termed as front end. It is also referred to as the ‘client side’ of the application. 
 It includes everything that users experience directly: text colors and styles, images, graphs and tables, buttons, colors, and navigation menu. 
 The structure, design, behavior, and content of everything seen on browser screen when websites, web applications, or mobile apps are opened up, 
 is implemented by front End developers. 
 Responsiveness and performance are two main objectives of the front End. The developer must ensure that the site is responsive i.e. it appears 
 correctly on devices of all sizes no part of the website should behave abnormally irrespective of the size of the screen.

#Some Front End Technologies:-

 1.HTML: 
  HTML stands for Hyper Text Markup Language. It is used to design the front end portion of web pages using markup language. 
  HTML is the combination of Hypertext and Markup language. Hypertext defines the link between the web pages. 
  The markup language is used to define the text documentation within tag which defines the structure of web pages.

 2.CSS: 
  Cascading Style Sheets fondly referred to as CSS is a simply designed language intended to simplify the process of making web pages presentable. 
  CSS allows you to apply styles to web pages. More importantly, CSS enables you to do this independent of the HTML that makes up each web page.

 3.JavaScript: 
  JavaScript is a famous scripting language used to create the magic on the sites to make the site interactive for the user. 
  It is used to enhancing the functionality of a website to running cool games and web-based software.

 4.AngularJS: 
  AngularJs is a JavaScript open source front-end framework that is mainly used to develop single page web applications(SPAs). 
  It is a continuously growing and expanding framework which provides better ways for developing web applications. 
  It changes the static HTML to dynamic HTML. It is an open source project which can be freely used and changed by anyone. 
  It extends HTML attributes with Directives, and data is bound with HTML.

 5.Bootstrap
  Bootstrap is the highly used front end technologies to develop responsive and mobile-first websites. It contains the most popular HTML, CSS, and JS framework. 
  They get the ease of using a predefined grid, which reduces their part of coding. It offers the following benefits to developers:-

   *Responsive Grid – It reduces the coding for the grid.
   *Responsive Images – Bootstrap comes with an in-built code that automatically resizes images based on the current screen size.
   *Components – Bootstrap contains a set of components that include navigation bars, dropdowns, progress bars, thumbnails, and more. 
    You can easily incorporate these elements into your webpage.
   *Uses JavaScript – It provides a dozen of JQuery plugins that allows web developers to build and deliver more interactive and easy solutions for modal popups, 
    transitions, image carousels, and more.

 6.Ajax
  AJAX stands for Asynchronous JavaScript and XML. AJAX is a new technique for creating better, faster, and more interactive web applications 
  with the help of XML, HTML, CSS, and Java Script.

   *Ajax uses XHTML for content, CSS for presentation, along with Document Object Model and JavaScript for dynamic content display.
   *Conventional web applications transmit information to and from the sever using synchronous requests. It means you fill out a form, 
    hit submit, and get directed to a new page with new information from the server.
   *With AJAX, when you hit submit, JavaScript will make a request to the server, interpret the results, and update the current screen. 
    In the purest sense, the user would never know that anything was even transmitted to the server.
   *XML is commonly used as the format for receiving server data, although any format, including plain text, can be used.
   *AJAX is a web browser technology independent of web server software.
   *A user can continue to use the application while the client program requests information from the server in the background.
   *Intuitive and natural user interaction. Clicking is not required, mouse movement is a sufficient event trigger.
   *Data-driven as opposed to page-driven.

 7.AMP
  AMP is an approach to mobile-friendly sites which stands for Accelerated Mobile Pages.This project, which is backed by Google, was created 
  as an open standard meant to allow website publishers to create sites that load more quickly on mobile devices.
  AMP is purely focused on speed — namely mobile speed. According to Malte Ubl, Google Tech Lead for this project, AMP aims to bring 
  “instant rendering to web content.” Some of the ways this is done include:

   *Lazy loading
   *Using preconnect
   *Prefetching resources
   *Async Javascript
   *Inline style sheets
   *No downloadable fonts
   *Resource prioritization

 8.Responsive Web Design
  Responsive Web design is the approach that suggests that design and development should respond to the user’s behavior and environment based on screen size, 
  platform and orientation.The practice consists of a mix of flexible grids and layouts, images and an intelligent use of CSS media queries. 
  As the user switches from their laptop to iPad, the website should automatically switch to accommodate for resolution, image size and scripting abilities.

#HTML
 Html is the language which is used widely to write web pages. It stands for Hyper-Text Markup Language. Any link which is available on webpages 
 is generally called as Hypertext and mark-up refers as a tag or page’s structure such that listed documents in webpages could be seen in a structured format. 
 The intent to develop HTML was to understand the structure of any documents which is heading, body, inner contents or paragraphs.

 =>Applications of HTML
  Wherever the web exists then it’s because of HTML. The application of HTML is disseminated across all electronics devices.

   *Browsers like Chrome, Firefox, Safari all use HTML to serve the web contents for better display.
   *Different mobile browsers like Opera, Firefox focus, Microsoft edge, dolphin, puffin, all are using HTML for better presentation and visibility 
    of internet contents in mobile.
   *Different smart devices are embedded with HTML function for better browsing and navigation during their operation.
   *Html supports primary authentication channel mechanism to any of the webpages so that unwanted traffic could be stopped.
   *Html accommodate the large content but gives the same visibility for small screen devices and large screen devices.

 
 =>Advantages of HTML

  *Html is a platform Independent.
  *It is widely and globally accepted.
  *Every browser supports HTML.
  *It is easy to learn, use and modify.
  *It is by default available in all of the browsers so no need to purchase and install.
  *Html is very useful for beginners in web designing field.
  *It supports a wide range of colors, formats, and layouts.
  *It uses the templates which makes the website design easier.

#CSS
 Cascading Style Sheets which is in a better way known as CSS, is a very simple designed process which is used for making the web pages a lot more presentable. 
 CSS allows you to put styles to customize your web pages. The best part about making use of this styling feature is that the CSS is independent of the HTML 
 way of creating web pages. The basic difference between the Hypertext Markup Language and the Cascading Style Sheets is that the former is mainly known to 
 provide the structural way of the landscape to the web page while the former is meant to provide a powerful color coding and styling techniques. 
 It is used to control the layout of more than one web page all at once. All the external stylesheets are stored in the form of CSS files.

 =>Applications of CSS
  There are three ways of HTML accessing CSS:

  1. Inline:
   An inline style sheet is only used to affect the tag it is in. This essentially means that the small details on the page can be changed without changing 
   the overall layout of the page or everything on the page. This is advantageous as if you had everything on the external pages, then you would be required to 
   add additional tags to change details. Inline overrules external which means that the small details can be changed. It also overrules the internal.

  2. Internal:
   The internal would only be used when you wanted to add a small change in the single tag. This is because inline only affects the one tag that is contained 
   within it whereas the internal styling is put on the head of the HTML document. This means that if you wish to customize the page, all the required changes 
   would be seen by just scrolling. The internal styling is placed inside the tags. Comparatively, this looks neater, simple, elegant and organized because of 
   the separate styling and tagging.

  3. External
   External stylesheets are used to allow people to format and recreate their webpages on an entirely different document. This effectively means that you can 
   have two or more workplaces, as more than one stylesheet can be embedded inside the document thereby providing you a much cleaner workspace. The stylesheet 
   would be easily accessible in this case which is a huge advantage but on the other hand, any changes done in the external sheet would affect all the parent 
   sheets it is linked to.

 =>Advantages of CSS

  *Device Compatibility
  *Faster website speed
  *Easily maintainable
  *Consistent and spontaneous changes
  *Ability to re-position
  *Enhances search engine capabilities to crawl the web pages

#JavaScript
 JavaScript as it is a lightweight dynamic programming language. Programs written in this language are called as scripts, these scripts are embedded in web pages 
 and they can manipulate the HTML content. These scripts execute as web pages are loaded, JavaScript’s do not need to have complied to execute. Previously JavaScript 
 was known as LiveScript, but later it was changed to JavaScript. As Java was very popular at that time and introducing a new language with the similarity in names 
 would be beneficial they thought. But later JavaScript became an independent language with its own specification called ECMAScript. 

 =>Why JavaScript Is Unique?
  There are various tools and programming languages available for the creation of web browsers. JavaScript is distinctive from any of them due to the presence of 
  three features which are uniquely combined. The incorporation of these three features together is made available only in JavaScript and this makes it a unique 
  programming language especially for interface design.

  *It provides complete integration with HTML. HTML is a standard tool for the development of web pages. Assimilation of JavaScript brings in powerful libraries 
   and tools to handle most of the required tasks.
  *Along with being integrated with HTML, it also is supported by all browsers which makes it the ultimate choice
  *The programming language is efficient with constantly evolving libraries. This makes JavaScript a high performing programming language in all areas of web, 
   mobile app developments, and even IOT.

 =>JavaScript Frameworks
  JavaScript is an integral part of web development or designing. However, with tons of code and multiple people working on it, it is not an easy task to manage the codes 
  without a suitable JavaScript Framework. Frameworks are being used because responsive design is a must if developing a web application or web site.

  1. Angular.js
   It is an open-source JavaScript Framework. Being Google Operated, it has fewer chances of bugs and errors from the coding aspect. This is mostly used to develop Single 
   Page Applications (SPA). HTML is great to define and show the static documents but when it comes to developing dynamic websites or applications, it does not have much 
   of the functions to be used for. This is where AngularJS comes into the picture. It improves the vocabulary of your application.

  2. Ember.js
   Though it can not be called very old as it started in 2015, it has grown a lot. Not only that, it has been very popular because of its wide application area. The best thing
   about this JavaScript Framework is, it supports two-way data binding. Because of it, it always establishes a reliable platform and handles the most complicated user 
   interface. Ember is a free open-source client-side JavaScript Framework licensed under MIT. Earlier the name of this Framework was SproutCore MVC network. 
   Ember is well-known for its reusability and it can maintain the JavaScript Web Application so well. It lowers the amount of work by managing the URLs easily. Not only that, 
   but it also has the functionality to change and update the entire model whenever the content is being changed.

  3. Meteor
   If you are thinking of making a full-stack JavaScript platform for making a web application or mobile application, Meteor could fit in. It helps to create real-time apps 
   because rather than combining so many different tools, it provides a full ecosystem. Though it can not be used for very large and complex applications, it offers great 
   support and starts for beginners and fresh coders. The best use of this is making real-time apps. It basically provides a platform so that cross-platform web, Android 
   and iOS applications could be developed. The good thing is the same code could be used for all types of devices and browsers. It also offers a great number of extension 

  4. Node.js
   In 2009, Ryan Dahl developed this server-side platform. This was basically built on Chrome’s JavaScript search engine. It is one of the most useful JavaScript Framework 
   because it is not only free and open-source licensed under MIT but also cross-platform. It can be used for developing network applications. Node.js is known for its speed
   which is why it is the most widely used framework for networking. It uses the single-threaded model which helps the server to respond smoothly in no time. This makes it 
   highly scalable and avails no-buffer in applications.

  5. Backbone.js
   Backbone.js is an open-source JavaScript Framework and you could avail hundreds of extensions to improve its functionality.  is used to make a client-side application and 
   it is known for its lightweight. The Model View Controller abstracts data into the model using events. Backbone.js provides you with an easier way to manage all of your 
   front-end development. As the name suggests, it provides the basic building blocks which assemble the backbone of client-side applications. Just like most of the framework,
   it also updates HTML elements along with model changes.packages which is very easy to install to improve the functionality of the Framework.

  =>JavaScript Tools

  1.React JS
   React JS is an open-source technology that was initially introduced as a JS library by Facebook engineers. It presents innovative ways to depict web pages with its many   
   advantages including high performance and a dynamic UI. In React, we can reuse the components of the code, which helps us in saving time and effort and this feature comes 
   especially handy when we have system updates. React components are distinct, thus they do not interfere with each other. We have several features to React. Data binding is 
   done in the downwards direction to empower a steady code. We have a virtual DOM which enables us to increase the speed. Continuous developments and enhancements by React JS
   community help in bringing it to the top.

  2.Express JS
   Express JS is a web framework that helps designers in building web pages and websites, by using the technology Node JS. Express JS provides us with strong tools that can 
   be used for HTTP servers, making it stable. Express JS can be used for routing. It includes great test coverage and has features like caching and redirection, which can 
   help with the HTTP servers. It generally executes applications and websites pretty quickly and supports numerous engines. We use Express JS because it is very simple to 
   build APIs in Express, making it easy to create websites and single-page applications.

  3.jQuery
   jQuery continues to be one of the most popular JavaScript tools available to date due to its correct syntax and small size. It is used to create client-side applications 
   and websites. It has great attributes like animations and event handling, and can also be used to create plugins on top of it. Companies like Google, DailyMotion and MSN 
   have used jQuery for building their websites.























